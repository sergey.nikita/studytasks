package by.intexsoft.nikita;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Configuration
@Scope("prototype")
@ConditionalOnProperty(prefix = "my.datasource", name = {"url", "username", "password"})
@EnableConfigurationProperties(DataBaseProperties.class)
public class PostgreSQLAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public DataBaseProperties dataBaseProperties() {
        return new DataBaseProperties();
    }

    @PostConstruct
    private void postConstruct() {
        System.out.println("bean created " + getClass());
    }

    @PreDestroy
    private void preDestroy() {
        System.out.println("bean will be destroyed " +  getClass());
    }

    public void sayHello(){
        System.out.println ("Hello");
    }

}
