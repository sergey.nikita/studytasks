package by.intexsoft.nikita.interfaces;

public interface Fillable {
    int[][] fill(int[][] array, int startStat);
}
