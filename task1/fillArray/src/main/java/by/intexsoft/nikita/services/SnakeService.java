package by.intexsoft.nikita.services;

import by.intexsoft.nikita.interfaces.Fillable;

public class SnakeService implements Fillable {

    public int[][] fill(int[][] array, int startIndex) {
        int increment = 1;
        return fillWithIncrement(array, startIndex, increment);
    }

    private int[][] fillWithIncrement(int[][] array, int startIndex, int increment) {
        for (int row = startIndex; row < array.length - startIndex; row++) {
            for (int column = startIndex; column < array.length - startIndex; column++) {
                if (row == startIndex) {
                    array[row][column] = increment++;
                } else {
                    column = array.length - startIndex - 1;
                    array[row][column] = increment++;
                }
            }
        }
        for (int row = array.length - startIndex - 1; row > startIndex; row--) {
            for (int column = array.length - startIndex - 2; column >= startIndex; column--) {
                if (row == array.length - startIndex - 1) {
                    array[row][column] = increment++;
                } else {
                    column = startIndex;
                    array[row][column] = increment++;
                }
            }
        }
        startIndex++;
        if (startIndex > array.length / 2)
            return array;
        else return fillWithIncrement(array, startIndex, increment);
    }
}
