package by.intexsoft.nikita;

import by.intexsoft.nikita.services.ConsolePrinter;
import by.intexsoft.nikita.services.PyramidService;
import by.intexsoft.nikita.services.SnakeService;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        SnakeService snake = new SnakeService();
        PyramidService pyramid = new PyramidService();
        ConsolePrinter consolePrinter = new ConsolePrinter();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input dimension of array");
        if (scanner.hasNextInt()) {
            int startStat = scanner.nextInt();
            int[][] arr = new int[startStat][startStat];
            consolePrinter.printArray(snake.fill(arr, 0));
            System.out.println();
            consolePrinter.printArray(pyramid.fill(arr, 0));
        }
    }
}
