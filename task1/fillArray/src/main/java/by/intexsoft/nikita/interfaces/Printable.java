package by.intexsoft.nikita.interfaces;

public interface Printable {
    void printArray(int[][] array);
}
