package by.intexsoft.nikita.services;

import by.intexsoft.nikita.interfaces.Printable;

public class ConsolePrinter implements Printable {
    public void printArray(int[][] array) {
        for (int[] ints : array) {
            for (int col = 0; col < array.length; col++) {
                System.out.print(ints[col] + "\t");
            }
            System.out.println();
        }
    }
}
