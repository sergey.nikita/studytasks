package by.intexsoft.nikita.services;

import by.intexsoft.nikita.interfaces.Fillable;

public class PyramidService  implements Fillable {

    public int[][] fill(int[][] array, int startIndex) {
        while (array.length / 2 >= startIndex) {
            for (int row = startIndex; row < array.length - startIndex; row++) {
                for (int column = startIndex; column < array.length - startIndex; column++) {
                    array[row][column] = startIndex + 1;
                }
            }
            startIndex++;
        }
        return array;
    }
}
