package by.intexsoft.nikita.interfaces;

import by.intexsoft.nikita.Dir;

import java.util.List;

public interface Creator {

    /**
     *
     * Method create for test this program list of directories, which childLists are empty.
     *
     * @return list of Dir
     */
    List<Dir> createDirTree();
}
