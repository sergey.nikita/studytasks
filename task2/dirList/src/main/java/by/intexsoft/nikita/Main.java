package by.intexsoft.nikita;

import by.intexsoft.nikita.interfaces.Creator;
import by.intexsoft.nikita.interfaces.Distributor;
import by.intexsoft.nikita.interfaces.Printer;
import by.intexsoft.nikita.service.DirTreeCreator;
import by.intexsoft.nikita.service.FolderDistributor;
import by.intexsoft.nikita.service.ListPrinter;

import java.util.List;

public class Main {
    public static void main(String[] args) {

        Creator dirTreeCreator = new DirTreeCreator();
        Printer listPrinter = new ListPrinter();
        Distributor folderDistributor = new FolderDistributor();

        List<Dir> exampleDirList = dirTreeCreator.createDirTree();
        listPrinter.print(exampleDirList);
        List <Dir> list = folderDistributor.distribute(exampleDirList);
        listPrinter.printDirTree(list);

    }
}
