package by.intexsoft.nikita.interfaces;

import by.intexsoft.nikita.Dir;

import java.util.List;

public interface Printer {
    /**
     * Method print list of Objects
     *
     * @param list - list for print
     */
    void print(List list);

    /**
     * Method print {@param dirList} as tree of directories.
     *
     * @param dirList - list of directories, which are sorted according parent directory's Id
     */
    void printDirTree(List<Dir> dirList);
}
