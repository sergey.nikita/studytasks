package by.intexsoft.nikita;

import java.util.LinkedList;
import java.util.List;

public class Dir {
    private static long count = 0;

    private long id;
    private String name;
    private long parentId;
    private List<Dir> children;

    public Dir() {
        count++;
        this.id = count;
        this.name = "dir" + count;
        this.children = new LinkedList<Dir>();

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getParentId() {
        return parentId;
    }

    public void setParentId(long parentId) {
        this.parentId = parentId;
    }

    public List<Dir> getChildren() {
        return children;
    }

    public void setChildren(List<Dir> children) {
        this.children = children;
    }

    @Override
    public String toString() {
        return "Dir{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", parentId=" + parentId +
                '}';
    }
}
