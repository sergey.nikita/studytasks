package by.intexsoft.nikita.service;

import by.intexsoft.nikita.Dir;
import by.intexsoft.nikita.interfaces.Printer;

import java.util.List;

public class ListPrinter implements Printer {

    /**
     * Method print list of Objects
     *
     * @param list - list for print
     */
    public void print(List list) {
        for (Object obj : list) {
            System.out.println(obj);
        }
        System.out.println();
    }

    /**
     * Method print {@param dirList} as tree of directories.
     *
     * @param dirList - list of directories, which are sorted according parent directory's Id
     */
    public void printDirTree(List<Dir> dirList) {
        dirList.sort((dir1, dir2) -> (int) (dir1.getId() - dir2.getId()));
        for (Dir dir : dirList) {
            printChild(dir, "");
        }
    }


    /**
     *
     * @param dir - directory, whose descendants need to be printed
     * @param tab
     */
    private void printChild(Dir dir, String tab) {
        System.out.print(tab + "Dir" + dir.getId() + " contains ");
        System.out.println(dir.getChildren());
        tab += "     ";
        if (!dir.getChildren().isEmpty()) {
            for (Dir dir1 : dir.getChildren()) {
                printChild(dir1, tab);
            }
        }
    }
}
