package by.intexsoft.nikita.interfaces;

import by.intexsoft.nikita.Dir;

import java.util.List;

public interface Distributor {

    /**
     * Method takes all directories from {@param dirList} and distribute them according parent directory's Id
     *
     * @param dirList - list of directories which must be distributed
     * @return tree of directories as list
     */
    List<Dir> distribute(List<Dir> dirList);
}
