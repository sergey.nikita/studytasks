package by.intexsoft.nikita.service;

import by.intexsoft.nikita.Dir;
import by.intexsoft.nikita.interfaces.Creator;

import java.util.LinkedList;
import java.util.List;

public class DirTreeCreator implements Creator {

    /**
     *
     * Method create for test this program list of directories, witch childLists are empty.
     *
     * @return list of Dir
     */

    public List<Dir> createDirTree() {
        List<Dir> dirList = new LinkedList<>();
        for (int index = 0; index < 28; index++) {
            dirList.add(new Dir());
        }
        for (
                Dir dir : dirList) {
            if (dir.getId() >= 2 && dir.getId() <= 4)
                dir.setParentId(1);
            else if (dir.getId() == 5 || dir.getId() == 6)
                dir.setParentId(2);
            else if (dir.getId() >= 7 && dir.getId() <= 9)
                dir.setParentId(3);
            else if (dir.getId() == 10)
                dir.setParentId(4);
            else if (dir.getId() == 11 || dir.getId() == 12)
                dir.setParentId(5);
            else if (dir.getId() == 13)
                dir.setParentId(6);
            else if (dir.getId() == 14)
                dir.setParentId(7);
            else if (dir.getId() == 15 || dir.getId() == 16)
                dir.setParentId(9);
            else if (dir.getId() == 17 || dir.getId() == 18)
                dir.setParentId(10);
            else if (dir.getId() == 28)
                dir.setParentId(18);
            else if (dir.getId() == 27)
                dir.setParentId(16);
            else if (dir.getId() == 25 || dir.getId() == 26)
                dir.setParentId(15);
            else if (dir.getId() == 23 || dir.getId() == 24)
                dir.setParentId(14);
            else if (dir.getId() == 21 || dir.getId() == 22)
                dir.setParentId(13);
            else if (dir.getId() == 20)
                dir.setParentId(12);
            else if (dir.getId() == 19)
                dir.setParentId(11);
        }
        return dirList;
    }
}
