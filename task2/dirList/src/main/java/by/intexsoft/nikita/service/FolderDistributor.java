package by.intexsoft.nikita.service;

import by.intexsoft.nikita.Dir;
import by.intexsoft.nikita.interfaces.Distributor;

import java.util.List;
import java.util.stream.Collectors;

public class FolderDistributor implements Distributor {

    /**
     * Method takes all directories from {@param dirList} and distribute them according parent directory's Id
     *
     * @param dirList - list of directories which must be distributed
     * @return tree of directories as list
     */
    public List<Dir> distribute(List<Dir> dirList) {
        for (Dir dir : dirList) {
            dirList.stream()
                    .filter(dir1 -> dir1.getParentId() == dir.getId())
                    .forEach(dir1 -> dir.getChildren().add(dir1));
        }
        return dirList.stream()
                .filter(dir1 -> dir1.getParentId() == 0)
                .collect(Collectors.toList());
    }
}
