package by.intexsoft.nikita.repository;

import by.intexsoft.nikita.entity.Employee;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EmployeeRepo extends CrudRepository<Employee, Long> {

    @Override
    @EntityGraph(attributePaths = {"projects"}, type = EntityGraph.EntityGraphType.FETCH)
    Optional<Employee> findById(Long id);

    @EntityGraph(attributePaths = {"address"}, type = EntityGraph.EntityGraphType.FETCH)
    Optional<Employee> getById(Long id);



}
