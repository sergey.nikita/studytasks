package by.intexsoft.nikita.service;

import by.intexsoft.nikita.entity.Project;
import by.intexsoft.nikita.repository.ProjectRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProjectService {

    @Autowired
    ProjectRepo projectRepo;

    private Project getProjectByIdWithoutAnything(Long id) {
        return projectRepo.findById(id).orElse(null);
    }

    private Project getProjectByIdWithEmployees(Long id) {
        return projectRepo.getById(id).orElse(null);
    }

    private List<Project> getAllProjects() {
        return projectRepo.findAll();
    }

}
