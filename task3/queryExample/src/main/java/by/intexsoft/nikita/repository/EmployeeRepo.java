package by.intexsoft.nikita.repository;

import by.intexsoft.nikita.entity.Employee;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public interface EmployeeRepo extends CrudRepository<Employee, Long> {

    @EntityGraph(attributePaths = {"projects"})
    Optional<Employee> findById(Long id);

    List<Employee> findAll();

    @EntityGraph(attributePaths = {"address"})
    Optional<Employee> getById(Long id);

    Optional<Employee> readById(Long id);
}
