package by.intexsoft.nikita.service;

import by.intexsoft.nikita.entity.Address;
import by.intexsoft.nikita.repository.AddressRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AddressService {

    @Autowired
    AddressRepo addressRepo;

    public Address getById(Long id) {
        return addressRepo.findById(id).orElse(null);
    }

    public List<Address> getAll(){
        return addressRepo.findAll();
    }
}
