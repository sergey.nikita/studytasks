package by.intexsoft.nikita.service;

import by.intexsoft.nikita.entity.Employee;
import by.intexsoft.nikita.repository.EmployeeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeService {
    @Autowired
    EmployeeRepo employeeRepo;

    public Employee getByIdWithProjects(Long id) {
        return employeeRepo.findById(id).orElse(null);
    }

    public Employee getByIdWithAddress(Long id){
        return employeeRepo.getById(id).orElse(null);
    }

    public Employee getByIdWithOutAnything(Long id){
        return employeeRepo.readById(id).orElse(null);
    }

}
