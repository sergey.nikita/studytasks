package by.intexsoft.nikita.repository;

import by.intexsoft.nikita.entity.Project;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProjectRepo extends CrudRepository<Project, Long> {

    Optional<Project> findById(Long id);

    @EntityGraph(attributePaths = {"employees"})
    Optional<Project> getById(Long id);

    List<Project> findAll();

}
