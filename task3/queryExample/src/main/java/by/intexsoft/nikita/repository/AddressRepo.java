package by.intexsoft.nikita.repository;

import by.intexsoft.nikita.entity.Address;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AddressRepo extends CrudRepository<Address, Long> {

    Optional<Address> findById(Long id);

    List<Address> findAll();


}
