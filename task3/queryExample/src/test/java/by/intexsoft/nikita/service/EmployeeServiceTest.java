package by.intexsoft.nikita.service;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EmployeeServiceTest {

    @Autowired
    EmployeeService employeeService;

    @Test
    public void shouldMakeQueryAndGetEmployeeWithProjectsByEmployeeId() {
        employeeService.getByIdWithProjects(3L);
    }

    @Test
    public void shouldMakeQueryAndGetEmployeeWithAddressByEmployeeId() {
        employeeService.getByIdWithAddress(3L);
    }

    @Test
    public void shouldMakeQueryAndGetEmployeeByIdWithOutAnything() {
        employeeService.getByIdWithOutAnything(3L);
    }
}